package prime.internship.micrometric.domain.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import prime.internship.micrometric.domain.user.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);

}
