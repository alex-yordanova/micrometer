package prime.internship.micrometric.domain.user.models;

public record UserGetDto(
    Long id,

    String email,

    String fullName,

    String location
) {

}
