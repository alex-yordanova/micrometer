package prime.internship.micrometric.domain.user.models;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public record UserCreateDto(
    @NotBlank(message = "An email is required.")
    @Email
    String email,

    @NotBlank(message = "A name is required.")
    @Size(min = 3, message = "Name should be at least 3 characters.")
    String fullName,

    @NotBlank(message = "A password is required")
    @Size(min = 8, message = "Password should be at least 8 characters.")
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$",
      message = "Password should contain 1 uppercase, 1 lowercase, 1 digit and a special symbol.")
    String password,

    @NotBlank(message = "A location is required")
    String location

) {
}
