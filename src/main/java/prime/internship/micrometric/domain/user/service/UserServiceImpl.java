package prime.internship.micrometric.domain.user.service;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import prime.internship.micrometric.domain.user.entity.User;
import prime.internship.micrometric.domain.user.mapper.UserMapper;
import prime.internship.micrometric.domain.user.models.UserCreateDto;
import prime.internship.micrometric.domain.user.models.UserGetDto;
import prime.internship.micrometric.domain.user.models.UserUpdateDto;
import prime.internship.micrometric.domain.user.repository.UserRepository;
import prime.internship.micrometric.infrastructure.exception.EntityAlreadyExistsException;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserMapper userMapper;

    private final MeterRegistry meterRegistry;

    private Counter deleteEntityNotFoundExceptionCounter;

    private Timer repositorySaveTimer;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper,
        MeterRegistry meterRegistry) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.meterRegistry = meterRegistry;

        setupMeters();
    }

    @Override
    public UserGetDto create(UserCreateDto userCreateDto) {
        if (userRepository.existsByEmail(userCreateDto.email())) {
            throw new EntityAlreadyExistsException(
                User.class.getSimpleName() + " with email " + userCreateDto.email()
                    + " already exists.");
        }
        User user = userMapper.userCreateDtoToUser(userCreateDto);

        Timer.Sample saveTimerSample = Timer.start(meterRegistry);
        User created = userRepository.save(user);
        saveTimerSample.stop(repositorySaveTimer);

        return userMapper.userToUserGetDto(created);
    }

    @Override
    public UserGetDto getById(Long id) {
        User user = userRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException(
                User.class.getSimpleName() + " with id " + id + " doesn't exist."));
        return userMapper.userToUserGetDto(user);
    }

    @Override
    public Page<UserGetDto> getAll(Pageable pageable) {
        return userRepository.findAll(pageable).map(userMapper::userToUserGetDto);
    }

    @Override
    public UserGetDto update(Long id, UserUpdateDto userUpdateDto) {
        User toUpdate = userRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException(
                User.class.getSimpleName() + " with id " + id + " doesn't exist."));
        userMapper.updateUserFromDto(userUpdateDto, toUpdate);

        Timer.Sample saveTimerSample = Timer.start(meterRegistry);
        User updated = userRepository.save(toUpdate);
        saveTimerSample.stop(repositorySaveTimer);

        return userMapper.userToUserGetDto(updated);

    }

    @Override
    public boolean deleteById(Long id) {
        if (!userRepository.existsById(id)) {
            deleteEntityNotFoundExceptionCounter.increment();
            throw new EntityNotFoundException(
                User.class.getSimpleName() + " with id " + id + " doesn't exist.");
        }

        userRepository.deleteById(id);
        return true;

    }

    private void setupMeters() {
        deleteEntityNotFoundExceptionCounter = Counter.builder(
                "user.delete.exceptionnotfound.counter")
            .tag("level", "service")
            .description(
                "Counts the number of EntityNotFound exceptions thrown by deleteById() method")
            .register(meterRegistry);

        repositorySaveTimer = Timer.builder("user.repository.save.timer")
            .tag("level", "service")
            .description("Measures the elapsed time of saving a user entity in the user database")
            .register(meterRegistry);

        Gauge.builder("user.repository.currentusercount", () -> userRepository.count())
            .tag("level", "service")
            .description("Current user count in database")
            .register(meterRegistry);
    }
}
