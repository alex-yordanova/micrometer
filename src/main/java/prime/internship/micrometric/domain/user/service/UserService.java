package prime.internship.micrometric.domain.user.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import prime.internship.micrometric.domain.user.models.UserCreateDto;
import prime.internship.micrometric.domain.user.models.UserGetDto;
import prime.internship.micrometric.domain.user.models.UserUpdateDto;

public interface UserService {

    UserGetDto create(UserCreateDto userCreateDto);

    UserGetDto getById(Long id);

    Page<UserGetDto> getAll(Pageable pageable);

    UserGetDto update(Long id, UserUpdateDto userUpdateDto);

    boolean deleteById(Long id);
}
