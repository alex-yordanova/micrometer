package prime.internship.micrometric.domain.user.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import prime.internship.micrometric.domain.user.entity.User;
import prime.internship.micrometric.domain.user.models.UserCreateDto;
import prime.internship.micrometric.domain.user.models.UserGetDto;
import prime.internship.micrometric.domain.user.models.UserUpdateDto;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User userCreateDtoToUser(UserCreateDto userCreateDto);

    UserGetDto userToUserGetDto(User user);

    void updateUserFromDto(UserUpdateDto userUpdateDto, @MappingTarget User user);
}
