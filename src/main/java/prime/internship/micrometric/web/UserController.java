package prime.internship.micrometric.web;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import prime.internship.micrometric.domain.user.models.UserCreateDto;
import prime.internship.micrometric.domain.user.models.UserGetDto;
import prime.internship.micrometric.domain.user.models.UserUpdateDto;
import prime.internship.micrometric.domain.user.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    private final MeterRegistry meterRegistry;

    private Counter getCallCounter;

    public UserController(UserService userService, MeterRegistry meterRegistry) {
        this.userService = userService;
        this.meterRegistry = meterRegistry;

        getCallCounter = Counter.builder("user.getbyid.counter")
            .tags("level", "controller")
            .description("Number of times GET users/{id} has been called")
            .register(meterRegistry);
    }

    @PostMapping
    @Timed(value = "user.create.time", description = "Time elapsed for creating a user")
    public ResponseEntity<UserGetDto> create(@Valid @RequestBody UserCreateDto userCreateDto) {
        return ResponseEntity.ok(userService.create(userCreateDto));
    }

    @GetMapping("/{id}")
    @Timed(value = "user.getbyid.time", description = "Time elapsed for getting a user by id")
    public ResponseEntity<UserGetDto> getById(@PathVariable Long id) {
        getCallCounter.increment();
        return ResponseEntity.ok(userService.getById(id));
    }

    @GetMapping
    @Timed(value = "user.getall.time", description = "Time elapsed for getting all users")
    public ResponseEntity<Page<UserGetDto>> get(Pageable pageable) {
        return ResponseEntity.ok(userService.getAll(pageable));
    }

    @PatchMapping("/{id}")
    @Timed(value = "user.update.time", description = "Time elapsed for updating a user")
    public ResponseEntity<UserGetDto> updateById(@PathVariable Long id,
        @RequestBody UserUpdateDto userUpdateDto) {
        return ResponseEntity.ok(userService.update(id, userUpdateDto));
    }

    @DeleteMapping("/{id}")
    @Timed(value = "user.delete.time", description = "Time elapsed for deleting a user")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public boolean delete(@PathVariable Long id) {
        return userService.deleteById(id);
    }
}
