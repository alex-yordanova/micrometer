package prime.internship.micrometric;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrometricApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrometricApplication.class, args);
	}

}
